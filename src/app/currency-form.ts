export class CurrencyForm {

    constructor(
        public id: number,
        public number: string,
        public currency : string,
        public decimalPoint : string
      ) {  }
}
