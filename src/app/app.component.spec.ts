import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import {CurrencyService} from './currency.service';
import { FormsModule,NgForm } from '@angular/forms';
import { By } from '@angular/platform-browser'; 
import { ComponentFixture} from '@angular/core/testing';
describe('AppComponent', () => {
  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent
      ],imports: [FormsModule],
      providers : [ CurrencyService]
    });
    // create component and test fixture
    fixture = TestBed.createComponent(AppComponent);
    
        // get test component from the fixture
        component = fixture.componentInstance;
        component.ngOnInit(); 

  }));
  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
 
  it('Should not accept currency in string format',async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    component.model.number = "currency";
  console.log(component.model.number)
  component.onSubmit();
    
    expect(component.submitted).toBe(false);
  }));
  it('Should not accept currency in negative number format ',async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const numberField = fixture.debugElement.query(By.css('input[name=number]'));
    //component.model.number = '-1';
  
  component.model.number = "-21";
  console.log(component.model.number)
  component.onSubmit();
    
    expect(component.submitted).toBe(false);
  }));
  it('Should accept currency in number format only',async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const numberField = fixture.debugElement.query(By.css('input[name=number]'));
    //component.model.number = '-1';
  
  component.model.number = "21";
  component.onSubmit();
    
    expect(component.submitted).toBe(true);
  }));
  it('Should accept currency in float number format also',async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const numberField = fixture.debugElement.query(By.css('input[name=number]'));
    //component.model.number = '-1';
  
  component.model.number = "21.34";
  component.onSubmit();
    
    expect(component.submitted).toBe(true);
  }));
});
