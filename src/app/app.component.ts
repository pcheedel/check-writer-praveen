import { Component } from '@angular/core';
import {CurrencyForm} from './currency-form';
import {CurrencyService} from './currency.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
 
  title = 'app';

  model : any;
  

   submitted = false;
  

  
  constructor(private currencyService : CurrencyService){
   
  }

  ngOnInit(){
    this.model = this.currencyService.getCurrencyForm(1);
  }
  
  // convert a number into "chunks" of 0-999
 
  
  onSubmit() {
    console.log("Wroking"+this.model.number)
    this.submitted = true;
    console.log(typeof(this.model.number)+"value --- "+isNaN(this.model.number));
    if(isNaN(this.model.number)){
      this.submitted = false;
      return this.submitted;
    }
    if(this.model.number <= 0){
      this.submitted = false;
      return this.submitted;
    }
    this.model.decimalPoint = this.currencyService.decimalPlaces(this.model.number);
    let number = parseInt(this.model.number);
    this.model.number = String(number);
    this.model.currency = this.currencyService.convertNumbersToCurrency(this.model.number)

    if(this.model.decimalPoint != ""){
    this.model.currency += " dollars and "+this.model.decimalPoint+"/100";
    }
    else{
      this.model.currency += " dollars only"
    }
 
   } 

   newForm() {
     this.model = this.currencyService.getCurrencyForm(2);
   }

   

   numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 46 || charCode > 57 || charCode == 47)) {
      return false;
    }
    return true;

  }
}
