import { Injectable } from '@angular/core';
import {CurrencyForm} from './currency-form';

@Injectable({
  providedIn: 'root'
})
export class CurrencyService {

  count = 0;
  
  ONE_TO_NINETEEN = [
    "one", "two", "three", "four", "five",
    "six", "seven", "eight", "nine", "ten",
    "eleven", "twelve", "thirteen", "fourteen", "fifteen",
    "sixteen", "seventeen", "eighteen", "nineteen"
  ];
  
   TENS = [
    "ten", "twenty", "thirty", "forty", "fifty",
    "sixty", "seventy", "eighty", "ninety"
  ];
  
   SCALES = ["thousand", "million", "billion", "trillion"];

   model : any;

  constructor() { }

  th = []

  getCurrencyForm(ID){
    this.model = new CurrencyForm(ID,"","","");
    return this.model;
  }

  decimalPlaces(num) {
    var match = (''+num).match(/(?:\.(\d+))?(?:[eE]([+-]?\d+))?$/);
    if (match[1] == undefined) { return 0; }
    console.log(match[1]);
    if(match[1] != undefined){
    this.model.decimalPoint = match[1].slice(0,2)
    }
    else{
      this.model.decimalPoint = "";
    }  
    
    return this.model.decimalPoint;
  }

  chunk(number) {
    var thousands = [];
  
    while(number > 0) {
      thousands.push(number % 1000);
      console.log("thousand -- "+thousands)
      number = Math.floor(number / 1000);
      console.log(" number -- "+number)
    }
    this.th = thousands
    console.log("Chunk will return at end --- "+thousands)
    return thousands;
  }

  // translate a number from 1-999 into English
  inEnglish(number) {
    var thousands, hundreds, tens, ones, words = [];

    if(number < 20) {
      return this.ONE_TO_NINETEEN[number - 1]; // may be undefined
    }
  
    if(number < 100) {
      ones = number % 10;
      tens = number / 10 | 0; // equivalent to Math.floor(number / 10)
      words.push(this.TENS[tens - 1]);
      words.push(this.inEnglish(ones));
      return words.filter(this.isTruthy).join("-");
    }
  
    hundreds = number / 100 | 0;
    words.push(this.inEnglish(hundreds));
    words.push("hundred");
    words.push(this.inEnglish(number % 100));
    
    return words.filter(this.isTruthy).join(" ");
  }
  
  // append the word for a scale. Made for use with Array.map
 appendScale(chunk, exp) {
  
   if(this.count <= exp ){
     exp = this.count;
   }
   console.log(chunk+" -appendSacle- "+exp)
    var scale;
    var SCALES = ["thousand", "million", "billion", "trillion"];
    this.count++;
    if(!chunk) {
      return null;
    }
    scale = SCALES[exp - 1];
    
    return [chunk, scale].filter(this.isTruthy).join(" ");
  }
  

   // helper function for use with Array.filter
   isTruthy(item) {
    return !!item;
  }


  convertNumbersToCurrency(number){
    this.count = 0;
    return this.chunk(number)
    .map(_data => this.inEnglish(_data))
    .map(_data => 
      this.appendScale(_data,this.th.length)
    )
    .reverse()
    .join(" ");
  }


}
