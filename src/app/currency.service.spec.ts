import { TestBed, inject } from '@angular/core/testing';

import { CurrencyService } from './currency.service';
import { CurrencyForm } from './currency-form';

describe('CurrencyService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CurrencyService]
    });
  });

  it('should be created', inject([CurrencyService], (service: CurrencyService) => {
    expect(service).toBeTruthy();
  }));

  describe('getCurrencyForm', () => {

    const ID: number = 1;

    it('should return CurrencyForm model', inject([CurrencyService], (service: CurrencyService) => {
      service.model = service.getCurrencyForm(ID);

      expect(service.model.id).toEqual(1);
    }));

  });

  describe('decimalPlaces', () => {

    beforeEach(() => {
      let num: number;
    })

    it('should return 0 if number is not decimal', inject([CurrencyService], (service: CurrencyService) => {
      this.num = 123;

      expect(service.decimalPlaces(this.num)).toBe(0);
    }));

    it('should return decimal place value if number is decimal', inject([CurrencyService], (service: CurrencyService) => {
      this.num = 123.123;
      service.model = new CurrencyForm(1,"","","");

      service.decimalPlaces(this.num);

      expect(service.model.decimalPoint).toBe("12");
    }));

  })

  describe('chunk', () => {

    it('should return array of 1000th place values', inject([CurrencyService], (service: CurrencyService) => {
      let num: number = 12345678;
      let chunks: number[] = service.chunk(num);

      // expect(chunks[0]).toBe(678);
      // expect(chunks[1]).toBe(345);
      // expect(chunks[2]).toBe(12);
      expect(chunks).toContain(678);
      expect(chunks).toContain(345);
      expect(chunks).toContain(12);
    }));

  })

  describe('inEnglish', () => {

      it('should return number in word if number is greater than 0 and less than 20', inject([CurrencyService], (service: CurrencyService) => {
        let number: number = 15;

        expect(service.inEnglish(number)).toBe('fifteen');
      }));

      it('should return number in words if number is greater than or equal to 20 and less than 100', inject([CurrencyService], (service: CurrencyService) => {
        let number: number = 86;

        expect(service.inEnglish(number)).toBe('eighty-six');
      }));

      it('should return number in words as "one hundred" if number is equal to 100', inject([CurrencyService], (service: CurrencyService) => {
        let number: number = 100;

        expect(service.inEnglish(number)).toBe('one hundred');
      }));

      it('should return number in words if number is greater than 100 and less than thousand', inject([CurrencyService], (service: CurrencyService) => {
        let number: number = 967;

        expect(service.inEnglish(number)).toBe('nine hundred sixty-seven');
      }));

  })

});
