"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
var core_1 = require("@angular/core");
var currency_form_1 = require("./currency-form");
require("rxjs/add/operator/map");
var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'app';
        this.model = new currency_form_1.CurrencyForm(18, "");
        this.submitted = false;
        this.ONE_TO_NINETEEN = [
            "one", "two", "three", "four", "five",
            "six", "seven", "eight", "nine", "ten",
            "eleven", "twelve", "thirteen", "fourteen", "fifteen",
            "sixteen", "seventeen", "eighteen", "nineteen"
        ];
        this.TENS = [
            "ten", "twenty", "thirty", "forty", "fifty",
            "sixty", "seventy", "eighty", "ninety"
        ];
        this.SCALES = ["thousand", "million", "billion", "trillion"];
    }
    // helper function for use with Array.filter
    AppComponent.prototype.isTruthy = function (item) {
        return !!item;
    };
    // convert a number into "chunks" of 0-999
    AppComponent.prototype.chunk = function (number) {
        var thousands = [];
        while (number > 0) {
            thousands.push(number % 1000);
            number = Math.floor(number / 1000);
        }
        return thousands;
    };
    // translate a number from 1-999 into English
    AppComponent.prototype.inEnglish = function (number) {
        var thousands, hundreds, tens, ones, words = [];
        console.log(number);
        if (number < 20) {
            return this.ONE_TO_NINETEEN[number - 1]; // may be undefined
        }
        if (number < 100) {
            ones = number % 10;
            tens = number / 10 | 0; // equivalent to Math.floor(number / 10)
            words.push(this.TENS[tens - 1]);
            words.push(this.inEnglish(ones));
            return words.filter(this.isTruthy).join("-");
        }
        hundreds = number / 100 | 0;
        words.push(this.inEnglish(hundreds));
        words.push("hundred");
        words.push(this.inEnglish(number % 100));
        return words.filter(this.isTruthy).join(" ");
    };
    // append the word for a scale. Made for use with Array.map
    AppComponent.prototype.appendScale = function (chunk, exp) {
        console.log(chunk);
        var scale;
        if (!chunk) {
            return null;
        }
        scale = this.SCALES[exp - 1];
        return [chunk, scale].filter(this.isTruthy).join(" ");
    };
    AppComponent.prototype.onSubmit = function () {
        var _this = this;
        this.submitted = true;
        this.model.number = this.chunk(this.model.number)
            .map(function (_data) { return _this.inEnglish(_data); })
            .map(this.appendScale)
            .filter(this.isTruthy)
            .reverse()
            .join(" ");
        console.log(this.inEnglish(this.chunk(this.model.number)));
    };
    AppComponent.prototype.newForm = function () {
        this.model = new currency_form_1.CurrencyForm(42, "");
    };
    AppComponent = __decorate([
        core_1.Component({
            selector: 'app-root',
            templateUrl: './app.component.html',
            styleUrls: ['./app.component.css']
        })
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;
