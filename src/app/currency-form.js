"use strict";
exports.__esModule = true;
var CurrencyForm = /** @class */ (function () {
    function CurrencyForm(id, number) {
        this.id = id;
        this.number = number;
    }
    return CurrencyForm;
}());
exports.CurrencyForm = CurrencyForm;
